import LottiePlayer from "./LottiePlayer";

export function Maintance() {
  return (
    <>
      <LottiePlayer />
      <span className="fw-bold">
        Estamos em manutenção para melhor atendê-lo.
      </span>
    </>
  );
}
