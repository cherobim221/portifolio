import Lottie from "react-lottie-player";
import loading from "./maintance.json";

function LottiePlayer() {
  return (
    <div className="d-flex flex-column align-items-center">
      <Lottie loop animationData={loading} play style={{ width: "30%" }} />
    </div>
  );
}

export default LottiePlayer;
