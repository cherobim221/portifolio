import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import { Maintance } from "./Maintance";
import { Logo } from "./Logo";

function App() {
  return (
    <div className="App">
      <Logo />
      <Maintance />
    </div>
  );
}

export default App;
